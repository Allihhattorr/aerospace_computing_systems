#!/bin/bash
# run with sudo

# https://forums.raspberrypi.com/viewtopic.php?t=141052
ip link set can0 up type can bitrate 125000

# auto start in boot time
echo 'auto can0' >> /etc/network/interfaces
echo 'iface can0 can static' >> /etc/network/interfaces
echo '    bitrate 125000' >> /etc/network/interfaces
